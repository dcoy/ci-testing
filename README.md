Badges:

[![coverage report](https://gitlab.com/dcoy/ci-testing/badges/master/coverage.svg)](https://gitlab.com/dcoy/ci-testing/-/commits/master)
[![pipeline status](https://gitlab.com/dcoy/ci-testing/badges/master/pipeline.svg)](https://gitlab.com/dcoy/ci-testing/-/commits/master)
[![Latest Release](https://gitlab.com/dcoy/ci-testing/-/badges/release.svg)](https://gitlab.com/dcoy/ci-testing/-/releases)

Just some CI Testing. Not meant for production usage. 

